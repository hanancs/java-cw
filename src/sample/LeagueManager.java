package sample;

import java.io.IOException;

public interface LeagueManager {



    public void createNewTeam();
    public void deleteExistingTeam();
    public void displayStats();
    public void displayLeagueTable();


}
