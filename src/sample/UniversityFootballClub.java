package sample;

public class UniversityFootballClub extends FootballClub{

    private String association;
    private String federation;

    public String getAssociation() {
        return association;
    }

    public void setAssociation(String association) {
        this.association = association;
    }

    public String getFederation() {
        return federation;
    }

    public void setFederation(String federation) {
        this.federation = federation;
    }
}
