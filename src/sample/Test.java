package sample;

import javafx.application.Application;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        PremierLeagueManager obj = new PremierLeagueManager(20);
        obj.retrieveAll();
            while (true) {
                System.out.println("...Premier League Manager...");
                System.out.println("Create a new team==> Press 1");
                System.out.println("Delete Team ==> Press 2");
                System.out.println("Display Various Stats of particular Club==> Press 3");
                System.out.println("Display league table ==> Press 4");
                System.out.println("Add match ==> Press 5");
                System.out.println("Match according to date ==> Press 6");
                System.out.println("GUI ==> Press 7");
                System.out.println("Exit ==> Press 8");
                Scanner input = new Scanner(System.in);
                String line = input.nextLine();

                switch (line) {
                    case "1":
                        obj.createNewTeam();
                        break;
                    case "2":
                        obj.deleteExistingTeam();
                        break;
                    case "3":
                        obj.displayStats();
                        break;
                    case "4":
                        obj.displayLeagueTable();
                        break;
                    case "5":
                        obj.addMatch();
                        break;
                    case "6":
                        obj.displayMatchOfDate();
                        break;
                    case "7":
                        try {
                            Application.launch(Main.class);
                        } catch (Exception exception) {
                            System.out.println("Exception");
                        }
                        break;
                    case "8":
                        obj.saveAll();
                        obj.exit();
                        break;
                    default:
                        System.out.println("invalid");

                }


            }
    }
}




