package sample;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;



public class Checker {


    public static void main(String[] args) throws ParseException, IOException {


        ArrayList<FootballClub> clubList;
        ArrayList<Match> matchList;
        clubList = new ArrayList<>();
        matchList = new ArrayList<>();

        FileInputStream fis = new FileInputStream("Data");
        ObjectInputStream ois = new ObjectInputStream(fis);
        for(;;){
            try{
                FootballClub club = (FootballClub)ois.readObject();
                clubList.add(club);
            } catch (EOFException | ClassNotFoundException eofException){
                break;
            }
        }
        fis.close();
        ois.close();
        System.out.println("All Club data are retried back");


        Random random = new Random();

        int randomNum = random.nextInt((10) + 1);
        int randomNum2 = random.nextInt((10) + 1);
        System.out.println(randomNum);
        System.out.println(randomNum2);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime date = LocalDateTime.now();
        int randy = random.nextInt(40+1)+6;
        LocalDateTime generatedDate = date.plusDays(randy);
        String formattedFutureLocalDateTime = generatedDate.format(dateTimeFormatter);
        System.out.println(formattedFutureLocalDateTime);

        String randomClubA = clubList.get(random.nextInt(clubList.size())).getClubName();
        System.out.println(randomClubA);
        String randomClubB = clubList.get(random.nextInt(clubList.size())).getClubName();
        System.out.println(randomClubB);

        Date date2;
        try {
           date2 = new SimpleDateFormat("dd/MM/yyyy").parse(formattedFutureLocalDateTime);
        } catch (ParseException ex) {
            System.out.println("Invalid input");
            return;
        }

        FootballClub home = null;
        for (FootballClub club : clubList) {
            if (club.getClubName().equals(randomClubA)) {
                home = club;

            }

        }
        if(home==null){
            System.out.println("Please insert an home club which is already participated");
            return;
        }

        FootballClub away = null;
        for (FootballClub club : clubList) {
            if (club.getClubName().equals(randomClubB)) {
                away = club;

            }
        }
        if(away==null){
            System.out.println("Please insert an away club which is already participated");
            return;
        }

        if(randomClubA.equals(randomClubB)){
            System.out.println("same team added twice");
            return;
        }

        int goalsH = -10;
        try {
            goalsH = Integer.parseInt(String.valueOf(randomNum));
        } catch (Exception e) {
            System.out.println("Invalid input");
        } if(goalsH==-10){
            System.out.println("please insert goals in numbers");
            return;
        }

        int goalsA = -10;
        try {
            goalsA = Integer.parseInt(String.valueOf(randomNum2));
        } catch (Exception ignored){

        }
        if (goalsA == -10) {
            System.out.println("please insert goals in numbers");
            return;
        }


        home.setGoals(home.getGoals()+goalsH);
        away.setGoals(away.getGoals()+goalsA);
        home.setGoalsConceded(home.getGoalsConceded()+ goalsA);
        away.setGoalsConceded(away.getGoalsConceded()+ goalsH);
        home.setPlayedMatches(home.getPlayedMatches()+1);
        away.setPlayedMatches(away.getPlayedMatches()+1);

        if(goalsH > goalsA){
            home.setWins(home.getWins()+1);
            away.setLosses(away.getLosses()+1);
            home.setPoints(home.getPoints()+3);
        } else if (goalsH < goalsA){
            away.setWins(away.getWins()+1);
            home.setLosses(home.getLosses()+1);
            away.setPoints(away.getPoints()+3);
        } else{
            home.setDraws(home.getDraws()+1);
            away.setDraws(away.getDraws()+1);
            home.setPoints(home.getPoints()+1);
            away.setPoints(away.getPoints()+1);
        }

        Match match = new Match();
        match.setClub1(home);
        match.setClub2(away);
        match.setClub1_scores(goalsH);
        match.setClub2_scores(goalsA);
        match.setDate(date2);

        matchList.add(match);

        FileOutputStream fos = new FileOutputStream("MatchData");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        for(Match match2:matchList){
            oos.writeObject(match2);
        }


        fos.close();
        oos.close();

        System.out.println("All data are saved");


    }
}
