package sample;

import java.util.Comparator;

public class DateComparator implements Comparator {

    public int compare(Object obj1, Object obj2) {
        Match m1 = (Match) obj1;
        Match m2 = (Match) obj2;

        if (m1.getDate().before(m2.getDate())) {
            return -1;
        } else if (m1.getDate().after(m2.getDate())) {
            return 1;
        } else {
            return 0;

        }
    }
}