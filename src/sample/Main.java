package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Main extends Application {

    private Scanner input;
    private ArrayList<FootballClub> clubList;
    private ArrayList<Match> matchList;

    public Main()  {
        clubList = new ArrayList<>();
        matchList = new ArrayList<>();
        input = new Scanner(System.in);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        FileInputStream fis = new FileInputStream("Data");
        ObjectInputStream ois = new ObjectInputStream(fis);
        for(;;){
            try{
                FootballClub club = (FootballClub)ois.readObject();
                clubList.add(club);
            } catch (EOFException eofException){
                break;
            }
        }
        fis.close();
        ois.close();
        System.out.println("All Club data are retried back");

        FileInputStream fis1 = new FileInputStream("MatchData");
        ObjectInputStream ois1 = new ObjectInputStream(fis1);

        for(;;){
            try{

                Match match = (Match) ois1.readObject();
                matchList.add(match);



            } catch (EOFException eofException){
                break;
            }
        }

        fis.close();
        ois.close();

        System.out.println("All Club data are retried back");

        Scene scene = new Scene(mainScreen(),800,600);
        primaryStage.setScene(scene);
        primaryStage.setTitle("League Manager App");
        primaryStage.show();

    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    public VBox mainScreen(){

        Label lbl1 = new Label("Choose a team type to add it to the respective league");
        Button btnClub = new Button("Club");
        btnClub.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnClub.getScene().setRoot(club());
            }
        });


        HBox root = new HBox();
        root.getChildren().addAll(btnClub);
        root.setSpacing(20);
        root.setAlignment(Pos.CENTER);

        VBox vb = new VBox();
        vb.getChildren().addAll(lbl1,root);
        vb.setSpacing(50);
        vb.setAlignment(Pos.CENTER);
        return vb;
    }

    public VBox club(){
        VBox vbAddTeam = new VBox();

        Button btnDisplayTable = new Button("Display Points Table");
        btnDisplayTable.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnDisplayTable.getScene().setRoot(displayTable());
            }
        });

        Button btnGenerator = new Button("Generate a random match");
        btnGenerator.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    generator();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });

        Button btnDateDisplay = new Button("All the Matches");
        btnDateDisplay.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnDateDisplay.getScene().setRoot(displayMatchwithdates());
            }
        });

        Button btnDateOfMatch = new Button("Match of Particular Date");
        btnDateOfMatch.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnDateOfMatch.getScene().setRoot(matchOfDate());
            }
        });
        Button btnBack = new Button("Back");
        btnBack.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                btnBack.getScene().setRoot(mainScreen());
            }
        });

        vbAddTeam.getChildren().addAll(btnDisplayTable, btnGenerator,btnDateDisplay,btnDateOfMatch,btnBack);
        vbAddTeam.setAlignment(Pos.CENTER);
        vbAddTeam.setSpacing(10);
        return vbAddTeam;
    }


    public GridPane displayTable(){
        GridPane gpDisplayTable = new GridPane();
        final String[] display1 ={""};
        TextArea taTable = new TextArea();
        taTable.setPrefHeight(500);
        taTable.setPrefWidth(650);

        Button backAddTeam = new Button("Back");
        clubList.sort(new FootballClubComparator());
        for (FootballClub club1 : clubList) {
            display1[0] = display1[0] +"\n"+ club1;
            taTable.setText(display1[0]);
        }

        Button sortGoals = new Button("Sort According to Goal");
        backAddTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backAddTeam.getScene().setRoot(club());
            }
        });

        sortGoals.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sortGoals.getScene().setRoot(displayTable2());
            }
        });

        Button sortLarge = new Button("Sort According to Largest Win");
        sortLarge.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                sortLarge.getScene().setRoot(displayTable3());
            }
        });
        VBox vbTable = new VBox();
        taTable.setEditable(false);
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backAddTeam,taTable,sortGoals,sortLarge);
        vbTable.getChildren().addAll(hBox,taTable);
        vbTable.setAlignment(Pos.CENTER);
        vbTable.setSpacing(10);
        gpDisplayTable.add(vbTable,0,1);
        gpDisplayTable.setAlignment(Pos.CENTER);
        return gpDisplayTable;
    }

    public GridPane displayTable2(){
        GridPane gpDisplayTable = new GridPane();
        final String[] display1 ={""};
        TextArea taTable = new TextArea();
        taTable.setPrefHeight(500);
        taTable.setPrefWidth(650);

        Button backAddTeam = new Button("Back");


        backAddTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backAddTeam.getScene().setRoot(displayTable());
            }
        });

        clubList.sort(new GoalComparator());
        for (FootballClub club : clubList){
            display1[0] = display1[0] +"\n"+ club;
            taTable.setText(display1[0]);

        }


        VBox vbTable = new VBox();
        taTable.setEditable(false);
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backAddTeam,taTable);
        vbTable.getChildren().addAll(hBox,taTable);
        vbTable.setAlignment(Pos.CENTER);
        vbTable.setSpacing(10);
        gpDisplayTable.add(vbTable,0,1);
        gpDisplayTable.setAlignment(Pos.CENTER);
        return gpDisplayTable;
    }

    public GridPane displayTable3(){
        GridPane gpDisplayTable = new GridPane();
        final String[] display1 ={""};
        TextArea taTable = new TextArea();
        taTable.setPrefHeight(500);
        taTable.setPrefWidth(650);

        Button backAddTeam = new Button("Back");


        backAddTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backAddTeam.getScene().setRoot(displayTable());
            }
        });

        clubList.sort(new LargestWinComparator());
        for (FootballClub club : clubList){
            display1[0] = display1[0] +"\n"+ club;
            taTable.setText(display1[0]);

        }

        VBox vbTable = new VBox();
        taTable.setEditable(false);
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backAddTeam,taTable);
        vbTable.getChildren().addAll(hBox,taTable);
        vbTable.setAlignment(Pos.CENTER);
        vbTable.setSpacing(10);
        gpDisplayTable.add(vbTable,0,1);
        gpDisplayTable.setAlignment(Pos.CENTER);
        return gpDisplayTable;
    }



    public GridPane displayMatchwithdates(){
        GridPane gpDisplayTable = new GridPane();
        final String[] display ={""};
        TextArea taTable = new TextArea();
        taTable.setPrefHeight(500);
        taTable.setPrefWidth(650);

        Button backAddTeam = new Button("Back");
        backAddTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backAddTeam.getScene().setRoot(club());
            }
        });

        matchList.sort(new DateComparator());
        for(Match match:matchList){
            display[0]=display[0]+
                        "\nDate: "+match.getDate()+
                        "\nHome: "+match.getClub1().getClubName()+
                        "\nAway: "+match.getClub2().getClubName()+
                        "\nHome Goals: "+match.getClub1_scores()+
                        "\nAway Goals: "+match.getClub2_scores();

            taTable.setText(display[0]);
        }


        VBox vbTable = new VBox();
        taTable.setEditable(false);
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backAddTeam,taTable);
        vbTable.getChildren().addAll(hBox,taTable);
        vbTable.setAlignment(Pos.CENTER);
        vbTable.setSpacing(10);
        gpDisplayTable.add(vbTable,0,1);
        gpDisplayTable.setAlignment(Pos.CENTER);
        return gpDisplayTable;
    }


    public GridPane matchOfDate(){
        TextArea textAreaMatchDate = new TextArea();
        final String[] display = {""};
        Label lblMatchDate = new Label("Enter the date");
        TextField tfMatchDate = new TextField();
        tfMatchDate.setPromptText("Type the date here...");
        Button enterDate = new Button("Enter");
        enterDate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Date date;
                try {
                    date = new SimpleDateFormat("dd/MM/yyyy").parse(tfMatchDate.getText());
                } catch (ParseException | NullPointerException e) {
                    textAreaMatchDate.setText("Invalid Input");
                    return;
                }
                for(Match match:matchList){
                    if (Objects.equals(match.getDate(), date)){
                        display[0]=display[0]+
                                "\nDate: "+match.getDate()+
                                "\nHome: "+match.getClub1()+
                                "\nAway: "+match.getClub2()+
                                "\nHome Goals: "+match.getClub1_scores()+
                                "\nAway Goals: "+match.getClub2_scores();

                        textAreaMatchDate.setText(display[0]);
                        return;

                    } else {
                        textAreaMatchDate.setText("No matches were played on that day");
                        return;
                    }
                }
            }
        });
        Button backAddTeam = new Button("Back");
        backAddTeam.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                backAddTeam.getScene().setRoot(club());
            }
        });
        HBox hBoxmatchdate = new HBox();
        hBoxmatchdate.getChildren().addAll(lblMatchDate,tfMatchDate);
        hBoxmatchdate.setSpacing(10);
        hBoxmatchdate.setAlignment(Pos.CENTER);
        VBox vBoxmatchdate = new VBox();
        vBoxmatchdate.getChildren().addAll(hBoxmatchdate,enterDate,textAreaMatchDate,backAddTeam);
        vBoxmatchdate.setAlignment(Pos.CENTER);
        vBoxmatchdate.setSpacing(10);
        GridPane gpMatchOfDate = new GridPane();
        gpMatchOfDate.add(vBoxmatchdate,0,1);
        gpMatchOfDate.setAlignment(Pos.CENTER);
        return gpMatchOfDate;
    }

    public void generator() throws IOException {

        Random random = new Random();

        int randomNum = random.nextInt((10) + 1);
        int randomNum2 = random.nextInt((10) + 1);
        System.out.println(randomNum);
        System.out.println(randomNum2);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime date = LocalDateTime.now();
        int randy = random.nextInt(40+1)+6;
        LocalDateTime generatedDate = date.plusDays(randy);
        String formattedFutureLocalDateTime = generatedDate.format(dateTimeFormatter);
        System.out.println(formattedFutureLocalDateTime);

        String randomClubA = clubList.get(random.nextInt(clubList.size())).getClubName();
        System.out.println(randomClubA);
        String randomClubB = clubList.get(random.nextInt(clubList.size())).getClubName();
        System.out.println(randomClubB);


        Date date2;
        try {
            date2 = new SimpleDateFormat("dd/MM/yyyy").parse(formattedFutureLocalDateTime);
        } catch (ParseException ex) {
            System.out.println("Invalid input");
            return;
        }

        FootballClub home = null;
        for (FootballClub club : clubList) {
            if (club.getClubName().equals(randomClubA)) {
                home = club;

            }

        }
        if(home==null){
            System.out.println("Please insert an home club which is already participated");
            return;
        }

        FootballClub away = null;
        for (FootballClub club : clubList) {
            if (club.getClubName().equals(randomClubB)) {
                away = club;

            }
        }
        if(away==null){
            System.out.println("Please insert an away club which is already participated");
            return;
        }

        if(randomClubA.equals(randomClubB)){
            System.out.println("same team added twice");
            return;
        }

        int goalsH = -10;
        try {
            goalsH = Integer.parseInt(String.valueOf(randomNum));
        } catch (Exception e) {
            System.out.println("Invalid input");
        } if(goalsH==-10){
            System.out.println("please insert goals in numbers");
            return;
        }

        int goalsA = -10;
        try {
            goalsA = Integer.parseInt(String.valueOf(randomNum2));
        } catch (Exception ignored){

        }
        if (goalsA == -10) {
            System.out.println("please insert goals in numbers");
            return;
        }


        home.setGoals(home.getGoals()+goalsH);
        away.setGoals(away.getGoals()+goalsA);
        home.setGoalsConceded(home.getGoalsConceded()+ goalsA);
        away.setGoalsConceded(away.getGoalsConceded()+ goalsH);
        home.setPlayedMatches(home.getPlayedMatches()+1);
        away.setPlayedMatches(away.getPlayedMatches()+1);

        if(goalsH > goalsA){
            home.setWins(home.getWins()+1);
            away.setLosses(away.getLosses()+1);
            home.setPoints(home.getPoints()+3);
        } else if (goalsH < goalsA){
            away.setWins(away.getWins()+1);
            home.setLosses(home.getLosses()+1);
            away.setPoints(away.getPoints()+3);
        } else{
            home.setDraws(home.getDraws()+1);
            away.setDraws(away.getDraws()+1);
            home.setPoints(home.getPoints()+1);
            away.setPoints(away.getPoints()+1);
        }

        Match match = new Match();
        match.setClub1(home);
        match.setClub2(away);
        match.setClub1_scores(goalsH);
        match.setClub2_scores(goalsA);
        match.setDate(date2);

        matchList.add(match);

        FileOutputStream fos = new FileOutputStream("MatchData");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        for(Match match2:matchList){
            oos.writeObject(match2);
        }


        fos.close();
        oos.close();

        System.out.println("All data are saved");
    }



}
